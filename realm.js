import Realm from 'realm';

class Person extends Realm.Object { }
Person.schema = {
  name: 'Person',
  properties: {
    userName: {
      type: 'string',
    },
    lastName: {
      type: 'string',
    },
    userEmail: {
      type: 'string',
    },
    verifyEmail: {
      type: 'string',
    },
    userPassword: {
      type: 'string',
    },
    userMNumber: {
      type: 'number',
    },
    option: {
      type: 'string',
    },

  },
};

return new Realm({
  schema: [Person],
});