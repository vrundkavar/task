import React, { useState, createRef } from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
// import Dropdown from 'react-dropdown';
// import Loader from './Components/Loader';
import { styles } from './signUpScreenStyle';
import DropDownPicker from 'react-native-dropdown-picker';
import loginScreen from './logInScreen';
import realm from './realm';
import { RealmProvider } from 'react-native-realm';
import Realm from 'realm';
// import { Dropdown } from 'react-native-material-dropdown';
const signUpScreen = ({ props, navigation }) => {
  const [userName, setUserName] = useState('');
  const [lastName, setLastName] = useState('');
  const [userEmail, setUserEmail] = useState('');
  const [verifyEmail, setVerifyEmail] = useState('');
  const [userMNumber, setUserMNumber] = useState('');
  const [userGender, setUserGender] = useState('');
  const [userPassword, setUserPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [errortext, setErrortext] = useState('');
  const [isRegistraionSuccess, setIsRegistraionSuccess] = useState(false);

  const [option, setOption] = useState([
    { value: 'Male', label: 'male' },
    { value: 'Female', label: 'female' },
    { value: 'Other', label: 'other' },
  ]);
  // const [data, setData] = useState([
  //  {
  //     value: 'Fruit',
  //     label: 'Banana'
  //   }, {
  //     value: 'Vegetable',
  //     label: 'Tomato'
  //   }, {
  //     value: 'Fruit',
  //     label: 'Pear'
  //   }
  // ]);

  const realm = require('realm');

  // const databaseOptions = {
  //   schema =[{
  //     name: 'User',
  //     properties: {
  //       userName: {
  //         type: 'string',
  //       },
  //       lastName: {
  //         type: 'string',
  //       },
  //       userEmail: {
  //         type: 'string',
  //       },
  //       verifyEmail: {
  //         type: 'string',
  //       },
  //       userPassword: {
  //         type: 'string',
  //       },
  //       userMNumber: {
  //         type: 'number',
  //       },
  //       option: {
  //         type: 'string',
  //       },

  //     },
  //   }],
  //   schemaVersion: 0
  // }

  const userSchema = {
    name: 'User',
    properties: {
      userName: {
        type: 'string',
      },
      lastName: {
        type: 'string',
      },
      userEmail: {
        type: 'string',
      },
      verifyEmail: {
        type: 'string',
      },
      userPassword: {
        type: 'string',
      },
      userMNumber: {
        type: 'number',
      },
      option: {
        type: 'string',
      },

    },
  };

// const register_user = () => {

    // Realm.open( {schema: [userSchema]})
    // .then(realm => {
    //  realm.write(() => {
    //   const userInfo= realm.create('User',
    //       {
    //         userName: 'vkk', lastName: 'patel', userEmail: 'vrundakavar@intesols.com.au',
    //         verifyEmail: 'vrundakavar@intesols.com.au',
    //         userPassword: 'vrunda@123 ', userMNumber: '9874563214', option: 'female'
    //       });

    //   });
    //   realm.close();
    // }

    // .catch(error => {
    //   console.log(error);
    // });
// };

  // const handleSubmitButton = () => {
  //   setErrortext('');
  //   let reg = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  //   if (userName == undefined || userName == '') {
  //     alert('Please Enter First Name');
  //     //  return;
  //   }
  //   else if (lastName == undefined || lastName == '') {
  //     alert('Please Enter Last Name');
  //     //  return;
  //   }
  //   else if (reg.test(userEmail) === false) {
  //     alert('Please Enter Email Address');
  //     // return;
  //   }
  //   else if (verifyEmail != userEmail) {
  //     alert('Please Enter Same Email Address');
  //     // return;
  //   }
  //   else if (userPassword.length < 8) {
  //     alert('Password must be 8 character');
  //     // return;
  //   }
  //   else if (userMNumber.length == 10) {
  //     alert('Please Enter 10 digit Mbile Number');
  //     // return;
  //   }
  //   else if (!option) {
  //     alert('Please Select Gender');
  //     // return;
  //   }
  // };
  if (isRegistraionSuccess) {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#307ecc',
          justifyContent: 'center',
        }}>
        {/* <Image
            source={require('../Image/success.png')}
            style={{
              height: 150,
              resizeMode: 'contain',
              alignSelf: 'center'
            }}
          /> */}
        <Text style={styles.successTextStyle}>
          {' Registration Successful'}
        </Text>
        <TouchableOpacity
          style={styles.buttonStyle}
          activeOpacity={0.5}
          onPress={() => props.navigation.navigate('LoginScreen')}>
          <Text style={styles.buttonTextStyle}>{'Login Now'}</Text>
        </TouchableOpacity>
      </View>
    );
  }



  return (
    <View style={styles.flex}>

      {/* <Loader loading={loading} /> */}
      <ScrollView
        keyboardShouldPersistTaps="handled"

        contentContainerStyle={{
          justifyContent: 'center',
          alignContent: 'center',
        }}>

        <View >
          <View style={styles.SectionStyle}>
            <TextInput
              style={styles.inputStyle}
              onChangeText={setUserName}
              value={userName}
              placeholder="Enter First Name"
              placeholderTextColor="#8b9cb5"

            />
          </View>
          <View style={styles.SectionStyle}>
            <TextInput
              style={styles.inputStyle}
              onChangeText={setLastName}
              value={lastName}
              placeholder="Enter Last Name"
              placeholderTextColor="#8b9cb5"

            />
          </View>
          <View style={styles.SectionStyle}>
            <TextInput
              style={styles.inputStyle}
              onChangeText={setUserEmail}
              value={userEmail}
              placeholder="Enter Email Address"
              placeholderTextColor="#8b9cb5"
              keyboardType="email-address"

            />
          </View>
          <View style={styles.SectionStyle}>
            <TextInput
              style={styles.inputStyle}
              onChangeText={setVerifyEmail}
              value={verifyEmail}
              placeholder="Verify Email Address"
              placeholderTextColor="#8b9cb5"
              keyboardType="email-address"

            />
          </View>
          <View style={styles.SectionStyle}>
            <TextInput
              style={styles.inputStyle}
              onChangeText={setUserPassword}
              value={userPassword}
              placeholder="Enter Password"
              placeholderTextColor="#8b9cb5"
              secureTextEntry={true}

            />
          </View>
          <View style={styles.SectionStyle}>
            <TextInput
              style={styles.inputStyle}
              onChangeText={setUserMNumber}
              value={userMNumber}
              placeholder="Enter Mobile Number"
              placeholderTextColor="#8b9cb5"
              keyboardType='number-pad'

            />
          </View>
          <View style={styles.SectionStyle}>
            {/* <Dropdown
        value={option}
        onChangeText={setOption}
        placeholder={'Please Select Your gender'}
        /> */}
            <DropDownPicker
              items={option}
              style={styles.dropdown}
              placeholderStyle={{ color: '#8b9cb5' }}
              containerStyle={{}}
              placeholder="Select your Gender"
              dropDownStyle={{ backgroundColor: 'red', color: 'red' }}
              onChangeItem={item => setOption(item)}
            />

            {/* <Dropdown
       value={data}
        // data={data}
        // pickerStyle={{borderBottomColor:'transparent',borderWidth: 0}}
        // dropdownOffset={{ 'top': 0 }}
        // containerStyle = {styles.dropdown}
        onChangeText={setData}
        placeholder={'Please Select Your gender'}

      /> */}
          </View>

          {errortext != '' ? (
            <Text style={styles.errorTextStyle}>
              {errortext}
            </Text>
          ) : null}
          <TouchableOpacity
            style={styles.buttonStyle}
            activeOpacity={0.5}
            onPress={alert('register')}
          // register_user()}
          // handleSubmitButton}
          >
            <Text style={styles.buttonTextStyle}>{'REGISTER'}</Text>
          </TouchableOpacity>
          <View style={styles.signContainer}>
            <Text style={styles.textStyle}> {"Already Have An Account?"}</Text>
            <TouchableOpacity onPress={() => navigation.navigate('LoginScreen')} >
              <Text style={styles.signUpView} >{" Login Here"}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};
export default signUpScreen;
