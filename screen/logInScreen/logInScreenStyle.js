import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    mainBody: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'white',
        alignContent: 'center',
    },
    headerView: {
        alignItems: 'center',
        flexDirection: 'column',
        // height:50,
        //  width:50,
    },
    textView: {
        fontSize: 30,
        color: '#4682B4',
    },
    SectionStyle: {
        //   flex:0.5,
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        marginLeft: 35,
        marginRight: 35,
        margin: 10,
        // backgroundColor:'white',
    },
    inputStyle: {
        flex: 1,
        textAlignVertical: 'center',
        paddingLeft: 15,
        paddingRight: 15,
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#dadae8',
        elevation: 3,
    },
    buttonStyle: {
        backgroundColor: '#4682B4',
        borderWidth: 0,
        color: '#FFFFFF',
        height: 40,
        alignItems: 'center',
        paddingVertical: 10,
        borderRadius: 30,
        marginLeft: 35,
        marginRight: 35,
        marginTop: 20,
    },
    buttonTextStyle: {
        color: 'white',
    },
    registerTextStyle: {
        textAlign: 'center',
        fontSize: 14,
        alignSelf: 'flex-end',
        padding: 10,
        paddingRight: 10,
        marginRight: 25,
    },
    bottomContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    shareView: {
        flexDirection: 'row',
        justifyContent: "center",
    },
    gmailView: {
        height: 40,
        width: 50,
        alignSelf: 'center',
        justifyContent: 'center',
        marginLeft: 10,
        marginRight: 20,
        marginVertical: 10,
    },
    gmailImage: {
        width: '100%',
        height: 40,
        resizeMode: 'cover',
        margin: 10,
        borderRadius: 30,
    },
    textContainer: {
        opacity: 10,
        color: '#808080',
    },
    signContainer: {
        // paddingHorizontal:35,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    signUpView: {
        borderBottomWidth: 1,
        color: '#0000CD',
        fontSize:18
    },
    textStyle:{
        fontSize:15

    },
    flex:{
        flex:1,
        // backgroundColor:'red',
        justifyContent:'center',

    },
    modalContainer:{
        backgroundColor:'white',
        paddingVertical:5,
        // borderRadius:10,
       },
       modalHeader:{
           flexDirection:'column',
       },
    textModal:{
        color:'black',
        textAlign:'center',
        color:'#4682B4',
        fontSize:25,
    },
    textModal2:{
        color:'black',
        textAlign:'center',
        // color:'#4682B4',
        fontSize:13,
        paddingHorizontal:5,
        // paddingLeft:20,
    },
    container:{
        flexDirection:'row',
        // textAlign:'center'
        justifyContent:'center',
        paddingVertical:5,
    },
    iconStyle:{
        fontSize:25,
    },
    textModalStyle:{
        textAlign:'center',
        textAlignVertical:'center',
    },
    containHeader:{
       alignItems:'flex-end',
        justifyContent:'flex-end',
    },
    // errorTextStyle: {
        //     color: 'red',
        //     textAlign: 'center',
        //     fontSize: 14,
        //   },
});