import React, { useState } from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  ScrollView,
  Image,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
  TouchableHighlight,
} from 'react-native';
import { styles } from './logInScreenStyle';
import signUpScreen from '../signUpScreen/signUpScreen';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/Ionicons';

// import AsyncStorage from '@react-native-community/async-storage';

// import Loader from './Components/Loader';

const LoginScreen = ({ navigation }) => {
  const [userEmail, setUserEmail] = useState('');
  const [userPassword, setUserPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [errortext, setErrortext] = useState('');
  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };
  //   const passwordInputRef = createRef();

    const handleSubmitPress = () => {
      setErrortext('');
      if (!userEmail) {
        alert('Please Enter Email');
        return;
      }
      if (!userPassword) {
        alert('Please Enter Password');
        return;
      }
    }
  
  return (
    // <View style={styles.flex}>
<>
      <View style={styles.mainBody}>

        {/* <Loader loading={loading} /> */}
        <View style={styles.scrollView}>
          <View style={styles.headerView}>
            <Image
              source={require('../../src/image/user.png')}
              style={{
                width: '30%',
                height: 100,
                resizeMode: 'cover',
                margin: 50,
              }}
            />
            <Text style={styles.textView} >{"Login here"}</Text>

          </View>
          <View style={styles.SectionStyle}>
            <TextInput
              style={styles.inputStyle}
              onChangeText={(UserEmail) =>
                setUserEmail(UserEmail)
              }
              placeholder="Enter Email" //dummy@abc.com
              placeholderTextColor="#8b9cb5"
              autoCapitalize="none"
              keyboardType="email-address"
              returnKeyType="next"
              // onSubmitEditing={() =>
              //   passwordInputRef.current &&
              //   passwordInputRef.current.focus()
              // }
              underlineColorAndroid="#f000"
              blurOnSubmit={false}
            />
          </View>
          <View style={styles.SectionStyle}>
            <TextInput
              style={styles.inputStyle}
              onChangeText={(UserPassword) =>
                setUserPassword(UserPassword)
              }
              placeholder="Enter Password" //12345
              placeholderTextColor="#8b9cb5"
              keyboardType="default"
              // ref={passwordInputRef}
              onSubmitEditing={Keyboard.dismiss}
              blurOnSubmit={false}
              secureTextEntry={true}
              underlineColorAndroid="#f000"
              returnKeyType="next"
            />
          </View>
          <TouchableOpacity
            style={styles.buttonStyle}
            activeOpacity={0.5}
            onPress={handleSubmitPress}
          >
            <Text style={styles.buttonTextStyle}>{'LOGIN'}</Text>

          </TouchableOpacity>
          <TouchableOpacity onPress={() => { !isModalVisible }}>
            <Text style={styles.registerTextStyle}>
              {'Forgot Password'}
            </Text>
          </TouchableOpacity>
          <View style={styles.bottomContainer}>
            <Text style={styles.textContainer}> {"-----Or Login With------"}</Text>
          </View>
          <View style={styles.shareView}>
            <TouchableOpacity style={styles.gmailView} >
              <Image
                source={require('../../src/image/facebook.jpg')}
                style={styles.gmailImage}
              />
            </TouchableOpacity>
            <TouchableOpacity style={styles.gmailView} >
              <Image
                source={require('../../src/image/whatsapp.png')}
                style={styles.gmailImage}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.signContainer}>
            <Text style={styles.textStyle}> {"Don't have a Account?"}</Text>
            <TouchableOpacity onPress={() => navigation.navigate('signUpScreen')} >
              <Text style={styles.signUpView} >{" SignUp"}</Text>
            </TouchableOpacity>
          </View>

        </View>

      </View>
      <Modal
        //  isVisible={!isModalVisible}
        onBackdropPress={() => {setModalVisible(false)}}
        onRequestClose={() => { 
           setModalVisible(!modalVisible)}}
      >
        <View style={styles.flex}>
          <View style={styles.modalContainer} >
            <TouchableOpacity style={styles.containHeader}  onPress={()=>{ setModalVisible(!isModalVisible)}} >
            <Icon name="ios-close" style={styles.iconStyle} />
           </TouchableOpacity>
            <View style={styles.modalHeader}>
              <Text style={styles.textModal}>{'Forgot Password'}</Text>
              <Text style={styles.textModal2}>{"Enter Your Email we'll Send You Link To Reset Your Password."}</Text>
            </View>
            <View style={styles.SectionStyle}>
              <TextInput
                style={styles.inputStyle}
                onChangeText={(UserEmail) =>
                  setUserEmail(UserEmail)
                }
                placeholder="Enter Email" //dummy@abc.com
                placeholderTextColor="#8b9cb5"
                autoCapitalize="none"
                keyboardType="email-address"
                returnKeyType="next"
                // onSubmitEditing={() =>
                //   passwordInputRef.current &&
                //   passwordInputRef.current.focus()
                // }
                underlineColorAndroid="#f000"
                blurOnSubmit={false}
              />
            </View>
            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
            onPress={()=>{}}
            >
             <Text style={styles.buttonTextStyle}>{'Submit'}</Text>
            </TouchableOpacity>
           
          <TouchableOpacity
            onPress={() =>navigation.navigate('signUpScreen')}
            style={styles.container}>
            <Icon name="ios-chevron-back-outline" style={styles.iconStyle} />
            <Text style={styles.textModalStyle}>{'Sign Up'}</Text>
          </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </>
  );
};
export default LoginScreen;

