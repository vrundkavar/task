import React from 'react';
import { FlatList, Text, View } from 'react-native';
// import Realm from 'realm';
let realm;

export default class ViewAllUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      FlatListItems: [],
    };
    var user_details = realm.objects('User');
    this.state = {
      FlatListItems: user_details,
    };
  }
  ListViewItemSeparator = () => {
    return (
      <View style={{ height: 0.5, width: '100%', backgroundColor: '#000' }} />
    );
  };
  render() {
    return (
      <View>
        <FlatList
          data={this.state.FlatListItems}
          ItemSeparatorComponent={this.ListViewItemSeparator}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => (
            <View style={{ backgroundColor: 'white', padding: 20 }}>
             
              <Text>Name: {item.userName}</Text>
              <Text>LastName: {item.lastName}</Text>
              <Text>Email: {item.userEmail}</Text>
              <Text>verifyEmail: {item.verifyEmail}</Text>
              <Text>Password: {item.userPassword}</Text>
              <Text>Number: {item.userMNumber}</Text>
              <Text>Gender: {item.option}</Text>
            </View>
          )}
        />
      </View>
    );
  }
}