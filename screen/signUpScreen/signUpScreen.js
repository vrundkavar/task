import React, { useState, createRef } from 'react';
import {
  Item,
  TextInput,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
// import Dropdown from 'react-dropdown';
// import Loader from './Components/Loader';
import { styles } from './signUpScreenStyle';
// import DropDownPicker from 'react-native-dropdown-picker';
import loginScreen from '../logInScreen/logInScreen';
import { Picker } from '@react-native-picker/picker';
//  import { Dropdown } from 'react-native-material-dropdown';
const signUpScreen = ({ props, navigation }) => {
  const [userName, setUserName] = useState('');
  const [lastName, setLastName] = useState('');
  const [userEmail, setUserEmail] = useState('');
  const [verifyEmail, setVerifyEmail] = useState('');
  const [userMNumber, setUserMNumber] = useState('');
  const [userGender, setUserGender] = useState('Select your Gender');
  const [open, setOpen] = useState(true);
  const [userPassword, setUserPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [errortext, setErrortext] = useState('');
  const [
    isRegistraionSuccess,
    setIsRegistraionSuccess
  ] = useState(false);
  const [selectedLanguage, setSelectedLanguage] = useState("gender");
  const [value, setValue] = React.useState('key1');
  const [Index, setIndex] = useState('2');
  const emailInputRef = createRef();
  const ageInputRef = createRef();
  const addressInputRef = createRef();
  const passwordInputRef = createRef();
  const [option, setOption] = useState([
    { value: 'Male', label: 'Male' },
    { value: 'Female', label: 'Female' },
    { value: 'Other', label: 'Other' },
  ]);
  // const [data, setData] = useState([
  //  {
  //     value: 'Fruit',
  //     label: 'Banana'
  //   }, {
  //     value: 'Vegetable',
  //     label: 'Tomato'
  //   }, {
  //     value: 'Fruit',
  //     label: 'Pear'
  //   }
  // ]);



  const handleSubmitButton = () => {
    setErrortext('');
    if (!userName) {
      alert('Please Enter First Name');
      return;
    }
    if (!lastName) {
      alert('Please Enter Last Name');
      return;
    }
    if (!userEmail) {
      alert('Please Enter Email Address');
      return;
    }
    if (!verifyEmail) {
      alert('Please Enter Verify Email Address');
      return;
    }
    if (!userPassword) {
      alert('Please Enter Password');
      return;
    }
    if (!userMNumber) {
      alert('Please Enter Mobile Number');
      return;
    }
    // if (!option) {
    //   alert('Please Select Gender');
    //   return;
    // }



  };
  if (isRegistraionSuccess) {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#307ecc',
          justifyContent: 'center',
        }}>
        {/* <Image
            source={require('../Image/success.png')}
            style={{
              height: 150,
              resizeMode: 'contain',
              alignSelf: 'center'
            }}
          /> */}
        <Text style={styles.successTextStyle}>
          {' Registration Successful'}
        </Text>
        <TouchableOpacity
          style={styles.buttonStyle}
          activeOpacity={0.5}
          onPress={() => props.navigation.navigate('LoginScreen')}>
          <Text style={styles.buttonTextStyle}>{'Login Now'}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  return (
    <View style={styles.flex}>

      {/* <Loader loading={loading} /> */}
      <ScrollView
        keyboardShouldPersistTaps="handled"

        contentContainerStyle={{
          justifyContent: 'center',
          alignContent: 'center',
        }}>

        <View >
          <View style={styles.SectionStyle}>
            <TextInput
              style={styles.inputStyle}
              onChangeText={setUserName}
              value={userName}
              placeholder="Enter First Name"
              placeholderTextColor="#8b9cb5"

            />
          </View>
          <View style={styles.SectionStyle}>
            <TextInput
              style={styles.inputStyle}
              onChangeText={setLastName}
              value={lastName}
              placeholder="Enter Last Name"
              placeholderTextColor="#8b9cb5"

            />
          </View>
          <View style={styles.SectionStyle}>
            <TextInput
              style={styles.inputStyle}
              onChangeText={setUserEmail}
              value={userEmail}
              placeholder="Enter Email Address"
              placeholderTextColor="#8b9cb5"
              keyboardType="email-address"

            />
          </View>
          <View style={styles.SectionStyle}>
            <TextInput
              style={styles.inputStyle}
              onChangeText={setVerifyEmail}
              value={verifyEmail}
              placeholder="Verify Email Address"
              placeholderTextColor="#8b9cb5"
              keyboardType="email-address"

            />
          </View>
          <View style={styles.SectionStyle}>
            <TextInput
              style={styles.inputStyle}
              onChangeText={setUserPassword}
              value={userPassword}
              placeholder="Enter Password"
              placeholderTextColor="#8b9cb5"
              keyboardType='number-pad'
              secureTextEntry={true}

            />
          </View>
          <View style={styles.SectionStyle}>
            <TextInput
              style={styles.inputStyle}
              onChangeText={setUserMNumber}
              value={userMNumber}
              placeholder="Enter Mobile Number"
              placeholderTextColor="#8b9cb5"
              keyboardType='number-pad'

            />
          </View>
          <View style={styles.dropDownStyle}>
            <Picker
              selectedValue={selectedLanguage}
              onValueChange={(itemValue, itemIndex) =>{
                setSelectedLanguage(itemValue);
                setIndex(itemIndex);}
              }
              style=
              {styles.pickerStyle}
              mode="dropdown"
              >
              {/* style=   */}
               
              <Picker.Item label="Male"  color='#8b9cb5' value="Male"  />
              <Picker.Item label="Female" color='#8b9cb5' value="Female" />

            </Picker>
            {/* <DropDownPicker
              items={option}
              setItems={setOption}
              open={open}
              setOpen={setOpen}
              style={styles.dropdown}
              value={userGender}
              setValue={setUserGender}
              defaultValue={userGender}
              placeholderStyle={{ color: '#8b9cb5' }}
              placeholder="Select your Gender"
              onChangeItem={item => setUserGender(item.label)}
              labelStyle={{
                color: '#8b9cb5',

              }}
              selectedLabelStyle={{ color: '#8b9cb5' }}
            /> */}


          </View>

          {errortext != '' ? (
            <Text style={styles.errorTextStyle}>
              {errortext}
            </Text>
          ) : null}
          <TouchableOpacity
            style={styles.buttonStyle}
            activeOpacity={0.5}
            onPress={handleSubmitButton}
          >
            <Text style={styles.buttonTextStyle}>{'REGISTER'}</Text>
          </TouchableOpacity>
          <View style={styles.signContainer}>
            <Text style={styles.textStyle}> {"Already Have An Account?"}</Text>
            <TouchableOpacity onPress={() => navigation.navigate('LoginScreen')} >
              <Text style={styles.signUpView} >{" Login Here"}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};
export default signUpScreen;
{/* <Dropdown
        data={option}
        // onChangeText={setOption}
        label={'Please Select Your gender'}
        /> */}

{/* <Dropdown
       value={data}
        // data={data}
        // pickerStyle={{borderBottomColor:'transparent',borderWidth: 0}}
        // dropdownOffset={{ 'top': 0 }}
        // containerStyle = {styles.dropdown}
        onChangeText={setData}
        placeholder={'Please Select Your gender'}

      /> */}